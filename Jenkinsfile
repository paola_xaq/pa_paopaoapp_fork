pipeline {
  agent any
  environment {
    DOCKER_HUB_CRED = credentials('docker_hub_pass')
    SONAR_TOKEN = credentials('SONAR_TOKEN')
    console = 'console'
    shortCommit=sh(returnStdout: true, script: "git log --format=medium -1 ${GIT_COMMIT}").trim()
  }
  stages {
    stage('commit') {
      steps {
        sh 'chmod +x gradlew'
        sh './gradlew clean build'
      }
      post {
        always {
            archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true
            junit 'build/test-results/test/*.xml'
        }
      }
    }
    stage('code quality') {
      steps {
        sh 'export SONAR_TOKEN=${SONAR_TOKEN}'
        sh './gradlew sonarqube'
      }
    }
    stage('package') {
      steps {
        sh 'docker build -t paopao-app:1.3 .'
      }
    }
    stage('publish') {
      steps {
        sh 'docker login -u "${DOCKER_HUB_CRED_USR}" -p "${DOCKER_HUB_CRED_PSW}"'
        sh 'docker tag paopao-app:1.3 paoxaq/paopao-app:1.3'
        sh 'docker push paoxaq/paopao-app:1.3'
      }
    }
    stage('deploy to dev') {
      steps {
        sh 'docker-compose -f docker-compose.yml up -d'
      }
    }
  }
  post {
    always {
      mail to: 'paola.xaq@gmail.com',
          subject: "BUILD #:  ${currentBuild.number} - STATUS: ${currentBuild.result}",
          body: "\n BUILD #:  ${currentBuild.number} \n\nCommit ID: ${shortCommit} \n\n \n\nLogs: ${currentBuild.absoluteUrl + console}"
    }
  }
}