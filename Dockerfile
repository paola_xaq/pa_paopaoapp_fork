FROM openjdk:11-jre-slim-sid
EXPOSE 8080
ADD build/libs/PaoPaoApp-1.0.1.jar /paopaoapp/PaoPaoApp-1.0.1.jar
WORKDIR /paopaoapp
ENTRYPOINT ["java", "-jar", "PaoPaoApp-1.0.1.jar"]