package org.fundacion.jala.converter.core.javacompiler;

import org.fundacion.jala.converter.core.exceptions.CompilerException;
import org.fundacion.jala.converter.core.parameter.JavaParameter;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class JavaCompilerTest {

    @Test(expected = NullPointerException.class)
    public void shouldReturnException() throws CompilerException {
        JavaCompiler javaCompiler = new JavaCompiler();
        javaCompiler.javaCompiler(null);
    }
}
