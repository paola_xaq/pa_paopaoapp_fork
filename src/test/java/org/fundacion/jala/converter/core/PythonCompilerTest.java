package org.fundacion.jala.converter.core;

import org.fundacion.jala.converter.core.exceptions.CompilerException;
import org.fundacion.jala.converter.core.parameter.PythonEnum;
import org.fundacion.jala.converter.core.parameter.PythonParameter;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PythonCompilerTest {

    @Test(expected = NullPointerException.class)
    public void shouldReturnException() throws CompilerException {
        PythonCompiler pythonCompiler = new PythonCompiler();
        pythonCompiler.compiler(null);
    }

    @Test @Ignore
    public void shouldMakePythonFile() throws CompilerException{
        String fileName = "filetocompile.py";
        String path = "/archive/storage/" + fileName;
        PythonCompiler pythonCompiler = new PythonCompiler();
        String actual = pythonCompiler.makePythonFile("print(\"Hello world\")");
        String expected = System.getProperty("user.dir") + path;
        assertEquals(expected, actual);
    }
}
